import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Signup from '@/components/Signup'
import MyLeagues from '@/components/MyLeagues'
import CreateTeam from '@/components/CreateTeam'
import ViewTeam from '@/components/ViewTeam'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      beforeEnter: AuthGuard
    },
    {
      path: '/signup',
      name: 'Signup',
      component: Signup
    },
    {
      path: '/myLeagues',
      name: 'MyLeagues',
      component: MyLeagues,
      beforeEnter: AuthGuard
    },
    {
      path: '/createTeam',
      name: 'CreateTeam',
      component: CreateTeam,
      beforeEnter: AuthGuard
    },
    {
      path: '/viewTeam',
      name: 'ViewTeam',
      component: ViewTeam,
      beforeEnter: AuthGuard
    }
  ],
  mode: 'history'
})