import { store } from '../vuexStore'

export default (to, from, next) => {
  if(store.getters.getuser) {
    next()
  }
  else {
    next('/signup')
  }
}
