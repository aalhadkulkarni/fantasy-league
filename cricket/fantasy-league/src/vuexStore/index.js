import Vue from 'vue'
import Vuex from 'vuex'
import * as firebase from 'firebase'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null,
    joinedLeagueId: null,
    playerJson: null,
    teams: null,
    userLeagues: null,
    userTeamObj: null,
    userTeamCreated: null,
    userAlreadyJoined: null,
    loading: null,
    error: null

  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    },
    setJoinedLeagueId (state, payload) {
      state.joinedLeagueId = payload
    },
    setPlayerJson (state, payload) {
      state.playerJson = payload
    },
    setTeams (state, payload) {
      state.teams = payload
    },
    setUserLeagues (state, payload) {
      state.userLeagues = payload
    },
    setUserTeamObj (state, payload) {
      state.userTeamObj = payload
    },
    setUserTeamCreated (state, payload) {
      state.userTeamCreated = payload
    },
    setUserAlreadyJoined (state, payload) {
      state.userAlreadyJoined = payload
    },
    setLoading (state, payload) {
      state.loading = payload
    },
    setError (state, payload) {
      state.error = payload
    }
  },
  actions: {
    signInUser ({commit}, payload) {
      var provider = new firebase.auth.GoogleAuthProvider()
      firebase.auth().signInWithPopup(provider)
    },
    autoSignIn ({commit}, payload) {
      const userInfoObj = {
        displayName: payload.displayName,
        email: payload.email,
        photoURL: payload.photoURL,
        uid: payload.uid
      }

      commit('setUser', {userInfo: userInfoObj})
    },
    logoutUser ({commit}, payload) {
      firebase.auth().signOut()
      commit('setUser', null)
    },
    joinLeague ({commit}, payload) {
      firebase.database().ref('/leagueJoiningCodes/' + payload.leagueCode).once('value')
      .then(
        leagueId => {
          commit('setJoinedLeagueId', leagueId.val())
        }
      )
    },
    getTeamsObj ({commit,getters}, payload) {
      var teamsObj;
      var playerJson = {}
      var tournamentId = null
      firebase.database().ref('teams').once('value')
      .then(
        teams => {
          teamsObj = teams.val()
          commit('setTeams', teamsObj)
          return teams
        }
      )
      .then(
        teams => {
          return firebase.database().ref('players').once('value')
         }
        )
        .then(
          players => {
            var roleVsPlayerObj = {}
            var playersObj = players.val();

            for (var key in playersObj) {
              if(!roleVsPlayerObj[playersObj[key].role]) {
                roleVsPlayerObj[playersObj[key].role] = {}
              }
              playersObj[key].teamName = teamsObj[playersObj[key].teamId].shortName
              roleVsPlayerObj[playersObj[key].role][key] = playersObj[key]
            }
            playerJson.roleVsPlayerObj = roleVsPlayerObj
            return playerJson
          }
        )
        .then(
          playerJson => {
            var leagueId = getters.getJoinedLeagueId
            return firebase.database().ref("/leagues/" + leagueId).once("value")
          }
        )
        .then(
          leagues => {
            tournamentId = leagues.val().tournamentId
            return tournamentId
          }
        )
        .then(
          tournamentId => {
              return firebase.database().ref("matches").once("value")
          }
        )
        .then(
          matches => {
            var matchesObj = matches.val()
            var matchesDetailsObj = {}
            for(var key in matchesObj) {
              if(matchesObj[key].tournamentId == tournamentId) {
                matchesDetailsObj[key] = {}
                matchesDetailsObj[key].team1 = teamsObj[matchesObj[key].team1Id]
                matchesDetailsObj[key].team2 = teamsObj[matchesObj[key].team2Id]
              }
            }
            playerJson.matchesDetailsObj = matchesDetailsObj
            commit('setPlayerJson', playerJson)
          }
        )
    },
    createTeam ({commit}, payload) {
      var fantasyTeamIdValue = null
      firebase.database().ref('fantasyTeams').push(payload)
      .then(
        fantasyTeams => {
          fantasyTeamIdValue = fantasyTeams.getKey()
          return fantasyTeamIdValue
        }
      )
      .then(
        fantasyTeamId => {
          return firebase.database().ref('/users/' + payload.userAccountId + "/teams").push(fantasyTeamId)
        }
      ).
      then(
        users => {
          return firebase.database().ref('/leagues/' + payload.leagueId +"/fantasyTeams").push(fantasyTeamIdValue)
        }
      ).
      then(
        leagues => {
          commit("setUserTeamCreated", true)
        }
      )
    },
    getUserLeagues ({commit, getters}, payload) {
      var userTeamsVal = []
      var userLeagues = []
      var userLeaguesObj = []
      firebase.database().ref("/users/" + getters.getuser.userInfo.uid).once("value")
      .then(
        userTeams => {
          for(var key in userTeams.val().teams) {
              userTeamsVal.push(userTeams.val().teams[key])
          }
          return userTeams
        }
      )
      .catch(
        error => {
          commit("setError","You have not joined any League")
        }
      )
      .then(
        userTeams => {
          return firebase.database().ref("/fantasyTeams").once("value")
        }
      )
      .then(
        fantasyTeams => {
          for(var i=0; i<userTeamsVal.length;i++){
              userLeagues.push(fantasyTeams.child(userTeamsVal[i]).val().leagueId)
          }
          return userLeagues
        }
      )
      .then(
        userLeagues => {
          return firebase.database().ref("/leagues").once("value")
        }
      )
      .then(
        leagues => {
          for(var i=0; i<userLeagues.length;i++){
              var leaguesObj = {}
              leaguesObj.name = leagues.child(userLeagues[i]).val().name
              leaguesObj.tournamentId = leagues.child(userLeagues[i]).val().tournamentId
              leaguesObj.leagueId = leagues.child(userLeagues[i]).val().id
              userLeaguesObj.push(leaguesObj)
          }
          return userLeaguesObj
        }
      )
      .then(
        userLeaguesObj => {
          return firebase.database().ref("/tournaments").once("value")
        }
      )
      .then(
        tournaments => {
          for(var i=0;i<userLeaguesObj.length;i++) {
            userLeaguesObj[i].format = tournaments.child(userLeaguesObj[i].tournamentId).val().format
            userLeaguesObj[i].startTime = tournaments.child(userLeaguesObj[i].tournamentId).val().startTime
          }
          commit('setUserLeagues', userLeaguesObj)
        }
      )
    },
    goToUserLeague ({commit, getters}, payload) {
      var userLeague = null
      var userTeam = null
      var userTeamObjVal = null
      var teamsObj = null

      firebase.database().ref("/leagues/" + payload.leagueId).once("value")
      .then(
        league => {
          userLeague = league
          return userLeague
        }
      )
      .then(
        userLeague => {
          return firebase.database().ref("/users/" + getters.getuser.userInfo.uid).once("value")
        }
      )
      .then(
        userTeams => {
          for(var key in userTeams.val().teams) {
              for(var key1 in userLeague.val().fantasyTeams) {
                if(userLeague.val().fantasyTeams[key1] === userTeams.val().teams[key]) {
                  userTeam = userLeague.val().fantasyTeams[key1]
                  break
                }
                if(userTeam) {
                  break
                }
              }
          }
          return userTeam
        }
      )
      .then(
        userTeam => {
          return firebase.database().ref("/fantasyTeams/" + userTeam).once("value")
        }
      )
      .then(
        userTeamObj => {
          userTeamObjVal = userTeamObj.val()
          return userTeamObj
        }
      )
      .then(
        userTeamObj => {
            return firebase.database().ref("players").once("value")
        }
      )
      .then(
        players => {
          var playersObj = players.val()
          userTeamObjVal.playersSelectedObj = {}
          for(var i=0; i<userTeamObjVal.activePlayers.length; i++) {
              userTeamObjVal.playersSelectedObj[userTeamObjVal.activePlayers[i]] = playersObj[userTeamObjVal.activePlayers[i]]
          }
          return userTeamObjVal
        }
      )
      .then(
        userTeamObjVal => {
          return firebase.database().ref("teams").once("value")
        }
      )
      .then(
        teams => {
          teamsObj = teams.val()
          for(var key in userTeamObjVal.playersSelectedObj) {
            userTeamObjVal.playersSelectedObj[key].teamName = teamsObj[userTeamObjVal.playersSelectedObj[key].teamId].shortName
          }
          console.log(userTeamObjVal);
          commit("setUserTeamObj", userTeamObjVal)
        }
      )
    },
    checkUserAlreadyJoined ({commit, getters}, payload) {
      var userTeamsVal = []
      var userLeagues = []
      firebase.database().ref("/users/" + getters.getuser.userInfo.uid).once("value")
      .then(
        userTeams => {
          for(var key in userTeams.val().teams) {
              userTeamsVal.push(userTeams.val().teams[key])
          }
          return userTeams
        }
      )
      .catch (
        error => {
          commit("setUserAlreadyJoined", false)
        }
      )
      .then(
        userTeams => {
          return firebase.database().ref("/fantasyTeams").once("value")
        }
      )
      .then(
        fantasyTeams => {
          for(var i=0; i<userTeamsVal.length;i++){
              userLeagues.push(fantasyTeams.child(userTeamsVal[i]).val().leagueId)
          }
          return userLeagues
        }
      )
      .then(
        userLeagues => {
          return firebase.database().ref("leagueJoiningCodes").once("value")
        }
      )
      .then(
        leagueCodes => {
          if(userLeagues.includes(leagueCodes.val()[payload.leagueCode])) {
            commit("setUserAlreadyJoined", true)
          }
          else {
            commit("setUserAlreadyJoined", false)
          }
        }
      )
    },
    destroyError ({commit, getters}, payload) {
      commit("setError", null)
    }
  },
  getters: {
    getuser (state, payload) {
      return state.user
    },
    getJoinedLeagueId (state, payload) {
      return state.joinedLeagueId
    },
    getPlayerJson (state, payload) {
      return state.playerJson
    },
    getTeams (state, payload) {
      return state.teams
    },
    getUserLeagues (state, payload) {
      return state.userLeagues
    },
    getUserTeamObj (state, payload) {
      return state.userTeamObj
    },
    getUserTeamCreated (state, payload) {
      return state.userTeamCreated
    },
    getUserAlreadyJoined (state, payload) {
      return state.userAlreadyJoined
    },
    getLoading (state, payload) {
      return state.loading
    },
    getError (state, payload) {
      return state.error
    }
  }
})
