import Vue from 'vue'
import App from './App'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import * as firebase from 'firebase'
import { store } from './vuexStore'

Vue.use(Vuetify, {
  theme: {
    primary: "#EF5350",
    secondary: "#4DB6AC",
    tertiary: "#F9FBE7",
    accent: "#CFD8DC",
    error: "#f44336",
    warning: "#ffeb3b",
    info: "#673AB7",
    success: "#4caf50"
  }
})

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
      el: '#app',
      router,
      store,
      render: h => h(App),
    created () {
  firebase.initializeApp(
      {
        apiKey: 'AIzaSyD6jiFzXv1Bhq9gt_6QrNYzN7-cSTdVwZw',
        authDomain: 'fantasy-league-b5923.firebaseapp.com',
        databaseURL: 'https://fantasy-league-b5923.firebaseio.com',
        projectId: 'fantasy-league-b5923',
        storageBucket: 'gs://fantasy-league-b5923.appspot.com/'
      }
  )
  firebase.auth().onAuthStateChanged((user) => {
    if(user) {
      this.$store.dispatch('autoSignIn', user)
    }
  })
}
})